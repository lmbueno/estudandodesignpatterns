﻿using Impostos.Interface;
using Impostos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STRATEGY
{
    class Program
    {
        static void Main(string[] args)
        {
            IImpostos iccp = new ICCP();
            IImpostos ikcv = new IKCV();

            CalculadorImpostos calculador = new CalculadorImpostos();

            Orcamento orcamento = new Orcamento(600);
            orcamento.AdicionaItem(new Item("CANETA", 200));
            orcamento.AdicionaItem(new Item("Onibus", 200));
            orcamento.AdicionaItem(new Item("LAPIS", 200));

            calculador.RealizaCalculo(orcamento, iccp);
            calculador.RealizaCalculo(orcamento, ikcv);

            Console.ReadKey();
        }
    }
}
