﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    public class Orcamento
    {
        public double valor { get; private set;}
        public List<Item> Itens { get; private set; }

        public Orcamento(double valor)
        {
            this.valor = valor;
            this.Itens = new List<Item>();
        }

        public void AdicionaItem(Item item)
        {
            Itens.Add(item);
        }
    }
}
