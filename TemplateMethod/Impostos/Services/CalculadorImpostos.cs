﻿using Impostos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    public class CalculadorImpostos
    {
        public void RealizaCalculo(Orcamento orcamento, IImpostos imposto)
        {
            double valor = imposto.CalcularImposto(orcamento);
            Console.WriteLine(valor);
        }
    }
}
