﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    class IKCV : TemplateDeImpostoConsicional
    {
        public override bool DeveUsarMaximaTaxacao(Orcamento orcamento)
        {
            return orcamento.valor > 500 && TemItemMaiorQue100Reais(orcamento);
        }

        public override double MaximaTaxacao(Orcamento orcamento)
        {
            return orcamento.valor * 0.10;
        }

        public override double MinimaTaxacao(Orcamento orcamento)
        {
            return orcamento.valor * 0.06;
        }

        public bool TemItemMaiorQue100Reais(Orcamento orcamento)
        {
            bool retorno = false;

            foreach(var item in orcamento.Itens)
            {
                if (item.Valor > 100)
                    retorno = true;
            }

            return retorno;
        }
    }
}
