﻿using Impostos.Interface;
using Impostos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STRATEGY
{
    class Program
    {
        static void Main(string[] args)
        {
            IImpostos icms = new ICMS();
            IImpostos iss = new ISS();
            IImpostos iccc = new ICCC();

            Orcamento orcamento = new Orcamento(10000.0);
            CalculadorImpostos calculadora = new CalculadorImpostos();

            calculadora.RealizaCalculo(orcamento, icms);
            calculadora.RealizaCalculo(orcamento, iss);
            calculadora.RealizaCalculo(orcamento, iccc);

            Console.ReadKey();
        }
    }
}
