﻿using Impostos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Interface
{
    public interface IImpostos
    {
        double CalcularImposto(Orcamento orcamento);
    }
}
