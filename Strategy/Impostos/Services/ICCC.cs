﻿using Impostos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    class ICCC : IImpostos
    {
        public double CalcularImposto(Orcamento orcamento)
        {
            if (orcamento.valor < 1000.00)
            {
                return orcamento.valor * 0.05;
            }
            else if (orcamento.valor >= 1000.00 && orcamento.valor <= 300.00)
            {
                return orcamento.valor * 0.07;
            }
            else
            {
                return orcamento.valor * 0.08 + 30;
            }
        }
    }
}
