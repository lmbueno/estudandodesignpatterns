﻿using Impostos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    public class ICMS : IImpostos
    {
        public double CalcularImposto(Orcamento orcamento)
        {
            return orcamento.valor * 0.05 + 50;
        }
    }
}
