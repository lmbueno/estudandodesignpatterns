﻿using Impostos.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Impostos.Services
{
    public class ISS : IImpostos
    {
        public double CalcularImposto(Orcamento orcamento)
        {
            return orcamento.valor * 0.06;
        }
    }
}
