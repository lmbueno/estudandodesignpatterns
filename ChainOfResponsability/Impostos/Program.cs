﻿using Impostos.Interface;
using Impostos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STRATEGY
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculadorDeDescontos calculador = new CalculadorDeDescontos();

            Orcamento orcamento = new Orcamento(100);
            orcamento.AdicionaItem(new Item("CANETA", 100));
            orcamento.AdicionaItem(new Item("Onibus", 250));
            orcamento.AdicionaItem(new Item("LAPIS", 100));

            double desconto = calculador.Calcula(orcamento);

            Console.WriteLine(desconto);

            Console.ReadKey();
        }
    }
}
